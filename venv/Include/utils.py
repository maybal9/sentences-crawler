#!/usr/bin/env python
# -*- coding: utf-8 -*-

import nltk
from nltk import tokenize
import nltk.data
import os
import csv


he_corpus_table = [["Language", "Source", "Sentence"]]
en_corpus_table = [["Language", "Source", "Sentence"]]


def parse_text_to_sentences(text):
    sentences = []
    list(map(lambda x: sentences.extend(x.replace("\n","\r").split("\r")),tokenize.sent_tokenize(text)))
    return sentences


def parse_text_array_to_sentences(text_array):
    sentences = []
    list(map(lambda x: sentences.extend(parse_text_to_sentences(x)), text_array))
    return sentences


def save_corpus_table_to_file(lang):
    file_name = lang+'_thousand_file.csv';
    full_path = 'C:\\work\\Corpus\\'+file_name;
    with open(full_path, 'w', newline='', encoding='utf8') as f:
        writer = csv.writer(f)
        if(lang=="he" or lang=="HE"): writer.writerows(he_corpus_table)
        if(lang=="en" or lang=="EN"): writer.writerows(en_corpus_table)


#writes to corpus table
def write_to_corpus_table(lang, source ,sentence):
    table_row = [lang, source, sentence]
    if(len(sentence)>20):
        if(lang=="he" or lang=="HE"): he_corpus_table.append(table_row)
        if(lang=="en" or lang=="EN"): en_corpus_table.append(table_row)


#writes a list of sentences to corpus table
def write_list_to_corpus_table(lang, source, sentences):
    for sentence in sentences:
        write_to_corpus_table(lang, source, sentence)


def append_point_at_end(sentence):
    if not (sentence.endswith(".") or sentence.endswith(". ") or
            sentence.endswith("?") or sentence.endswith("? ")):
        sentence = sentence + "."
    return  sentence