#!/usr/bin/env python
# -*- coding: utf-8 -*-

import nltk
from nltk import tokenize
import nltk.data
import os
import csv
import wikipedia
from bs4 import BeautifulSoup
import requests
import feedparser as fp
import json
import newspaper
from newspaper import Article
from time import mktime
from datetime import datetime
from utils import *

#scrape sentences from international news sites
def scrape_international_news_sites(limit):
    # Set the limit for number of articles to download
    LIMIT = limit
    # Loads the JSON files with news sites
    with open('NewsPapers.json') as data_file:
        companies = json.load(data_file)
    count = 1
    # Iterate through each news company
    for company, value in companies.items():
        # If a RSS link is provided in the JSON file, this will be the first choice.
        # Reason for this is that, RSS feeds often give more consistent and correct data.
        # If you do not want to scrape from the RSS-feed, just leave the RSS attr empty in the JSON file.
        if 'rss' in value:
            d = fp.parse(value['rss'])
            for entry in d.entries:
                # Check if publish date is provided, if no the article is skipped.
                # This is done to keep consistency in the data and to keep the script from crashing.
                if hasattr(entry, 'published'):
                    if count > LIMIT:
                        break
                    try:
                        content = Article(entry.link)
                        content.download()
                        content.parse()
                    except Exception as e:
                        # If the download for some reason fails (ex. 404) the script will continue downloading
                        # the next article.
                        print(e)
                        print("continuing...")
                        continue
                    sentences = parse_text_to_sentences(content.title)
                    write_list_to_corpus_table("EN", "News", sentences)
                    sentences = parse_text_to_sentences(content.text)
                    write_list_to_corpus_table("EN", "News", sentences)

                    count = count + 1
        else:
            # This is the fallback method if a RSS-feed link is not provided.
            # It uses the python newspaper library to extract articles
            print("Building site for ", company)
            paper = newspaper.build(value['link'], memoize_articles=False)
            noneTypeCount = 0
            for content in paper.articles:
                if count > LIMIT:
                    break
                try:
                    content.download()
                    content.parse()
                except Exception as e:
                    print(e)
                    print("continuing...")
                    continue
                # Again, for consistency, if there is no found publish date the article will be skipped.
                # After 10 downloaded articles from the same newspaper without publish date, the company will be skipped.
                if content.publish_date is None:
                    noneTypeCount = noneTypeCount + 1
                    if noneTypeCount > 10:
                        print("Too many noneType dates, aborting...")
                        noneTypeCount = 0
                        break
                    count = count + 1
                    continue
                sentences = parse_text_to_sentences(content.title)
                write_list_to_corpus_table("EN", "News", sentences)
                sentences = parse_text_to_sentences(content.text)
                write_list_to_corpus_table("EN", "News", sentences)
                count = count + 1
                noneTypeCount = 0
        count = 1


#scrape sentences from a specific tapuz message
def scrape_sentences_tapuz_forums(url):
    data = requests.get(url)
    soup = BeautifulSoup(data.text, "lxml")
    msg_content = soup.find("div", class_="msg-body").find("div", class_="textContent").get_text()
    write_list_to_corpus_table("HE", "Tapuz", parse_text_to_sentences(msg_content))


#scrape sentences from english wiki article, without first sentence
def scrape_sentences_wikipedia_en(page_name):
    try:
        page = wikipedia.WikipediaPage(title=page_name)
        summary = page.summary
        sentences = parse_text_to_sentences(summary)
        sentences.pop(0)
        write_list_to_corpus_table("EN", "Wikipedia", sentences)
    except wikipedia.exceptions.DisambiguationError:
        return




#scrape sentences from random english wiki articles
def scrape_random_sentences_wikipedia_en(num_of_pages):
    page_names = [wikipedia.random(20) for i in range(num_of_pages)]
    for p in page_names:
        scrape_sentences_wikipedia_en(p)

#scrape headers and subheader of ynet article
def scrape_ynet_article(url):
    data = requests.get(url)
    soup = BeautifulSoup(data.text, "lxml")
    header_content = soup.find("div", class_="art_header_title")
    if header_content!=None:
        header = header_content.get_text()
        header = append_point_at_end(header)
        write_to_corpus_table("HE", "Ynet", header)
    sub_header_content = soup.find("div", class_="art_header_sub_title")
    if sub_header_content!=None:
        sub_header = sub_header_content.get_text()
        sub_header = append_point_at_end(sub_header)
        write_to_corpus_table("HE", "Ynet", sub_header)


#scrape ynet articles from front page
def scrape_front_page_ynet():
    url = "https://www.ynet.co.il/home/0,7340,L-8,00.html"
    data = requests.get(url)
    soup = BeautifulSoup(data.text, "lxml")
    msg_content = soup.find_all("a", class_="mta_title")
    msg_content_text = list(map(lambda x: x.get_text(), msg_content))
    msg_link_articles = list(map(lambda x: x.get('href'), msg_content))
    for msg in msg_content_text:
        write_to_corpus_table("HE", "Ynet", msg)
    for link_end in msg_link_articles:
        if link_end.startswith("https://") or link_end.startswith("http://"):
            link_url = link_end
        else:
            link_url = url[0: 22] + link_end
        scrape_ynet_article(link_url)


def scrape_Tapuz_Blog(url):
    data = requests.get(url)
    soup = BeautifulSoup(data.text, "lxml")
    text = []
    articles_content = soup.find_all("div", class_="content")
    for article_content in articles_content:
        for child in article_content.contents:
            if child.name == 'p':
                content = child.get_text()
                if len(content)>5:
                    text.append(content)

    sentences = parse_text_array_to_sentences(text)
    for sentence in sentences:
        write_to_corpus_table("HE", "Tapuz", sentence)


def scrape_behadrei_hadarim(url):
    data = requests.get(url)
    soup = BeautifulSoup(data.text, "lxml")
    text = []
    article_content = soup.find("div", class_="text-html")
    for child in article_content.descendants:
        if child.name == 'p':
            content = child.get_text()
            if len(content)>5:
                text.append(content)

    sentences = parse_text_array_to_sentences(text)
    for sentence in sentences:
        write_to_corpus_table("HE", "Behadrei Haredim", sentence)


#scrape thinkIL
def scrape_thinkIL(url):
    data = requests.get(url)
    soup = BeautifulSoup(data.text, "lxml")
    content = soup.find("div", {"id":"content"}).get_text()
    write_list_to_corpus_table("HE", "ThinkIL", parse_text_to_sentences(content))


#scrape gutenberg selected, from package nltk
def scrape_gutenberg_selected():
    dir = "C:\\nltk_data\\corpora\\gutenberg"
    for file in os.listdir(dir):
        if file.endswith(".txt"):
            selected = nltk.data.load("nltk:corpora/gutenberg/"+file)
            selected = selected.replace("'\n", "'.").replace("?\n", "?.").replace(")\n", ").").replace("\n", " ")
            ans = parse_text_to_sentences(selected)
            for line in ans:
                if (line.endswith(".") or line.endswith(". ") or line.endswith("?") or line.endswith("? ")):
                    write_to_corpus_table("EN", "Gutenberg Project", line)


def scrape_random_blog():
    url = "http://randomest.blogspot.com/"
    data = requests.get(url)
    soup = BeautifulSoup(data.text, "lxml")
    posts_content = soup.find_all("div", class_="post-body")
    for post_content in posts_content:
        post_text = post_content.get_text()
        sentences = parse_text_to_sentences(post_text)
        for sentence in sentences:
            lines = sentence.split(".")
            for line in lines:
                if len(line) > 10:
                    write_to_corpus_table("EN", "Random Blog", line)



#scrape state union, from package nltk
def scrape_state_union():
    dir = "C:\\nltk_data\\corpora\\state_union"
    for file in os.listdir(dir):
        if file.endswith(".txt"):
            selected = nltk.data.load("nltk:corpora/state_union/" + file)
            write_list_to_corpus_table("EN", "state_union", parse_text_to_sentences(selected))

#scrape kippa forum
def scrape_kippa_forum(url):
    data = requests.get(url)
    soup = BeautifulSoup(data.text, "lxml")
    msgs = soup.find_all("div", class_="subject")
    msg_subject = list(map( lambda x: x.get_text(), msgs))
    write_list_to_corpus_table("HE", "Kippa Forums", parse_text_array_to_sentences(msg_subject))

#scrape one sports site
def scrape_one_sports(url):
    data = requests.get(url)
    soup = BeautifulSoup(data.text, "lxml")
    msgs = soup.find_all("a", class_="one-article one-article-plain")
    msg_subject = list(map( lambda x: x.get_text(), msgs))
    write_list_to_corpus_table("HE", "One sports", parse_text_array_to_sentences(msg_subject))
    msgs = soup.find_all("a", class_="one-article one-article-secondary")
    msg_subject = list(map( lambda x: x.get_text(), msgs))
    write_list_to_corpus_table("HE", "One sports", parse_text_array_to_sentences(msg_subject))


