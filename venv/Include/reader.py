import csv
import pandas as pd
import random
from crawler import *
from utils import *


HE_corpus_table = [["Language","Source","Sentence"]]
EN_corpus_table = [["Language","Source","Sentence"]]

#writes to corpus table
def write_line_to_corpus(line):
    lang = line[0]
    if(lang=="en" or lang=="EN"): EN_corpus_table.append(line)
    if (lang == "he" or lang == "HE"): HE_corpus_table.append(line)

#writes table to file
def save_EN_corpus_table_to_file():
    with open('C:\\work\\Corpus\\en_random_corpus.csv', 'w', newline='', encoding='utf8') as f:
        writer = csv.writer(f)
        writer.writerows(EN_corpus_table)

#writes table to file
def save_HE_corpus_table_to_file():
    with open('C:\\work\\Corpus\\he_random_corpus.csv', 'w', newline='', encoding='utf8') as f:
        writer = csv.writer(f)
        writer.writerows(HE_corpus_table)

def save_corpus_to_file():
    save_EN_corpus_table_to_file()
    save_HE_corpus_table_to_file()


def get_all_sources(file_name):
    file_path = 'C:\\work\\Corpus\\' + file_name
    df = pd.read_csv(file_path)
    sources = df.Source
    sources_set = set()
    for idx,s in enumerate(sources):
        sources_set.add(sources[idx])
    return sources

def scrape_all_HEB_sources():
    tapuz_url = "http://www.tapuz.co.il/forums/viewmsg/2294/184270624/%D7%90%D7%A0%D7%A9%D7%99%D7%9D_%D7%95%D7%97%D7%91%D7%A8%D7%94/%D7%A6%D7%99%D7%A4%D7%95%D7%A8%D7%99_%D7%9C%D7%99%D7%9C%D7%94"
    scrape_sentences_tapuz_forums(tapuz_url)
    tapuz_url = "http://www.tapuz.co.il/forums/viewmsg/229/184283154/%D7%98%D7%99%D7%95%D7%9C%D7%99%D7%9D_%D7%95%D7%A4%D7%A0%D7%90%D7%99/%D7%98%D7%99%D7%95%D7%9C%D7%99%D7%9D_%D7%95%D7%97%D7%95%D7%A4%D7%A9%D7%95%D7%AA_%D7%91%D7%97%D7%95%D7%9C"
    scrape_sentences_tapuz_forums(tapuz_url)
    tapuz_url = "http://www.tapuz.co.il/forums/forumpage/110"
    scrape_sentences_tapuz_forums(tapuz_url)

    haredim_url = "https://www.bhol.co.il/news/1003866"
    scrape_behadrei_hadarim(haredim_url)
    haredim_url = "https://www.bhol.co.il/news/1002167"
    scrape_behadrei_hadarim(haredim_url)
    haredim_url = "https://www.bhol.co.il/news/1005657"
    scrape_behadrei_hadarim(haredim_url)

    tapuz_url = "http://www.tapuz.co.il/blogs/userblog/rodefet"
    scrape_Tapuz_Blog(tapuz_url)
    tapuz_url = "http://www.tapuz.co.il/blogs/userblog/shoshanpere"
    scrape_Tapuz_Blog(tapuz_url)

    scrape_front_page_ynet()

    scrape_thinkIL("http://thinkil.co.il/texts/m067p018-033/")
    scrape_thinkIL("http://thinkil.co.il/texts/m048p030-037/")
    scrape_thinkIL("http://thinkil.co.il/texts/m045p003-011/")
    scrape_thinkIL("http://thinkil.co.il/texts/m018p005-017/")
    scrape_thinkIL("http://thinkil.co.il/texts/m038p003-009/")

    scrape_kippa_forum("https://www.kipa.co.il/community/41")
    scrape_one_sports("https://www.one.co.il/")


def scrape_all_ENG_sources():
    scrape_sentences_wikipedia_en('Theresa_May')
    scrape_sentences_wikipedia_en('Bastille')
    scrape_sentences_wikipedia_en('Taneti Vanitha')
    scrape_sentences_wikipedia_en('The Approach')
    scrape_sentences_wikipedia_en('Sports in New Haven, Connecticut')
    scrape_sentences_wikipedia_en('1995 NCAA Men\'s Volleyball Tournament')
    scrape_sentences_wikipedia_en('Chagda, Aldansky District, Sakha Republic')
    scrape_sentences_wikipedia_en('List of French monarchs')
    scrape_sentences_wikipedia_en('Germania Superior')
    scrape_sentences_wikipedia_en('Habsburg Netherlands')
    scrape_random_sentences_wikipedia_en(20)

    scrape_international_news_sites(100)

    scrape_state_union()
    scrape_gutenberg_selected()
    scrape_random_blog()


def scrape_all_sources():
    scrape_all_HEB_sources()
    scrape_all_ENG_sources()


def load_corpus_random_sentences(file_name):
    file_path = 'C:\\work\\Corpus\\'+file_name
    sources = get_all_sources(file_name)
    sources = set(sources)
    with open(file_path, newline='', encoding="utf8") as csvfile:
        data = csv.reader(csvfile)
        num_of_sources = len(sources)
        num_sentences_each_source = int((200 + num_of_sources) / num_of_sources)
        for source in sources:
            print(source)
            source_rows = list()
            for row in data:
                if row[1] == source:
                    source_rows.append(row)
            print(len(source_rows))
            for i in range(num_sentences_each_source):
                if(len(source_rows)>0):
                    random.shuffle(source_rows)
                    line = source_rows.pop(0)
                    write_line_to_corpus(line)
            source_rows.clear()
            csvfile.seek(0)


scrape_all_HEB_sources()
save_corpus_table_to_file("he")
he_file_name = 'he_test_file.csv'
load_corpus_random_sentences(he_file_name)

# scrape_all_ENG_sources()
# save_corpus_table_to_file("en")
# en_file_name = 'en_thousand_file.csv'
# load_corpus_random_sentences(en_file_name)
save_corpus_to_file()